-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2020 at 06:51 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75', 'Jakarta- Makanan tradisonal khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI ke-75'),
(2, 'test', 'test.', NULL, NULL, NULL, 'test'),
(3, 'Dr.', 'Est accusamus molestias laborum deleniti voluptas molestias. Vel nesciunt harum sed nihil sunt. A consequatur rerum exercitationem. Impedit nobis ex quo iusto cumque placeat mollitia.', '2020-09-16 00:08:07', '2020-09-16 00:08:07', NULL, 'Grounds Maintenance Worker'),
(4, 'Ms.', 'Sit repudiandae neque perferendis repellendus accusantium similique qui. Ut molestiae est quia est perspiciatis adipisci animi amet. Rem dolorum voluptatem eum est repellat cum dicta.', '2020-09-16 00:08:07', '2020-09-16 00:08:07', NULL, 'Dragline Operator'),
(5, 'Prof.', 'Ut ipsum cupiditate et asperiores vitae. A consequatur sint qui et. Non labore mollitia dignissimos quisquam voluptas corporis dolorem alias.', '2020-09-16 00:08:07', '2020-09-16 00:08:07', NULL, 'Sociology Teacher'),
(6, 'Prof.', 'Ipsa aspernatur cupiditate et delectus tempore est. Possimus nemo deserunt molestiae voluptas. Dolor debitis repudiandae aperiam sit velit labore modi. Labore voluptatem aperiam consequatur.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Tax Examiner'),
(7, 'Dr.', 'Molestias ut rerum velit provident quae expedita. Voluptates qui est ut perspiciatis adipisci.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Transportation Equipment Painters'),
(8, 'Mrs.', 'Consequatur atque fugit neque odio ut est. Labore qui ratione nam inventore quae fugiat voluptas. Quia itaque eaque in temporibus fugiat ut repudiandae. Doloremque error voluptate eaque repellendus similique quia.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Surgical Technologist'),
(9, 'Mrs.', 'Quis voluptatibus exercitationem repudiandae incidunt. Illo minus omnis ex sequi ullam recusandae. At in maxime qui rerum. Laudantium quaerat quia rem qui.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Environmental Science Technician'),
(10, 'Miss', 'Explicabo alias dolorem magnam consequuntur consequatur distinctio quia. Consequuntur omnis sint delectus at commodi. Inventore eaque ut occaecati ab et hic autem.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Aircraft Rigging Assembler'),
(11, 'Dr.', 'Aut quia quisquam rem qui voluptatem cupiditate odio in. Qui et et atque qui nesciunt quam rem. Sit sint voluptatem et deserunt ullam.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Computer Scientist'),
(12, 'Prof.', 'Officia perferendis dicta commodi quo vel dolor iste. Veniam minima ipsum quia reiciendis recusandae amet ea. Natus magni aut omnis adipisci nisi.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Offset Lithographic Press Operator'),
(13, 'Miss', 'Non eum ut eligendi. Nihil laboriosam aut quis ut.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Sales Person'),
(14, 'Mr.', 'Dignissimos voluptatem excepturi ut dolor et. Qui sunt et voluptas ut. Aut et est dolorem tempore minima.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Grinding Machine Operator'),
(15, 'Mrs.', 'Sint voluptatem tenetur ut aspernatur tempore adipisci possimus similique. In enim dolorem quaerat dolore aut ea repellendus. Nihil sit qui non minus maxime perferendis aspernatur. Et voluptatum sed corporis est labore voluptatem nulla.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Philosophy and Religion Teacher'),
(16, 'Prof.', 'Mollitia deserunt commodi et omnis et similique itaque. Ratione placeat reprehenderit qui quidem libero. Rerum et quo eum quibusdam dignissimos.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Industrial-Organizational Psychologist'),
(17, 'Mr.', 'Sit suscipit id sed quia. Voluptatibus quos nobis voluptate odit tempora quibusdam placeat. Ullam ut quibusdam sed natus.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Heat Treating Equipment Operator'),
(18, 'Mrs.', 'Et optio accusamus ut odit ut dignissimos. Et in cumque placeat assumenda.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Textile Worker'),
(19, 'Dr.', 'Nihil quam reiciendis nesciunt a autem vero. Cumque quas totam unde et et tempora qui. Iste autem vitae voluptatem eum consequatur tenetur enim necessitatibus. Et autem fugiat sunt corporis recusandae.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Photographic Process Worker'),
(20, 'Mrs.', 'Accusamus omnis ut earum ut sunt et. Impedit optio et odio ipsum quibusdam et. Sed distinctio quasi voluptatibus qui. Et cumque dignissimos molestiae eveniet.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Pharmaceutical Sales Representative'),
(21, 'Mrs.', 'Autem laborum ipsa maiores consequatur est maxime aut. Magnam id ut ut iste magnam. Rerum voluptatem nostrum provident quasi. Rerum autem reprehenderit voluptatem omnis corporis atque.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Logging Equipment Operator'),
(22, 'Dr.', 'Illo quo eligendi sed quibusdam exercitationem aliquid delectus. Optio odit quos est qui sit. Ut ratione et ea sint quibusdam.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Logging Supervisor'),
(23, 'Mr.', 'Non ea odit vel illum iure. Quam ut voluptas aut officiis nobis. Aliquam et mollitia et et non dicta quis.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Oil and gas Operator'),
(24, 'Prof.', 'Quaerat beatae veniam facilis voluptas quia. Fuga aut autem consequatur libero rerum. Error amet fugit repellat dolores quos odit.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Gas Compressor Operator'),
(25, 'Dr.', 'Quaerat sit itaque ut perferendis nihil aut. Dolores inventore exercitationem in nihil quam. Commodi ea minima reiciendis aut autem temporibus eum.', '2020-09-16 00:08:08', '2020-09-16 00:08:08', NULL, 'Physicist'),
(26, 'Dr.', 'Autem voluptatibus aut enim quod. Accusantium in id porro natus asperiores eum. Quas mollitia sunt tempore aliquid accusamus.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Librarian'),
(27, 'Dr.', 'Sit molestiae aspernatur eveniet accusamus. Id rerum consequatur modi ut. Est fugiat et accusantium qui ut. Sequi expedita excepturi placeat quo distinctio.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Singer'),
(28, 'Mrs.', 'Veritatis harum voluptate velit harum. Natus dolor quaerat beatae fugit consectetur.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Carpenter Assembler and Repairer'),
(29, 'Prof.', 'Fuga omnis sint nemo repellat et dolores. Velit sed at perferendis vel earum omnis. Quo sunt ipsam voluptas assumenda. Soluta nemo qui itaque et.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Agricultural Equipment Operator'),
(30, 'Mrs.', 'Quo omnis iusto dicta corporis magnam. Vel mollitia illo iure iusto atque. Culpa itaque voluptate accusamus ab.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Credit Checkers Clerk'),
(31, 'Ms.', 'Et nostrum accusamus provident voluptatibus sit est et. Similique ea voluptatem sunt vitae est ut aliquam sit. Magni placeat illum ad quae. Est dolor consectetur qui est aperiam quidem vel.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Plant Scientist'),
(32, 'Dr.', 'Aliquid soluta voluptas praesentium laborum consequatur velit. Rerum molestiae amet perspiciatis eum illum in. Quasi ut quae deleniti et recusandae. Sequi dolores sit quidem aliquid blanditiis et sequi.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Library Science Teacher'),
(33, 'Dr.', 'Ipsa ut dolores velit. Asperiores laboriosam quia delectus magnam sit animi illum. Asperiores dolorem ad fugit laboriosam molestiae sed ut molestiae. Laboriosam odit quia eveniet ipsa.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Building Cleaning Worker'),
(34, 'Ms.', 'Autem numquam dolor qui vitae minus non quia. Voluptatum iusto eos in quia nisi id. Est ea ex et odio suscipit.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Boat Builder and Shipwright'),
(35, 'Mr.', 'Beatae asperiores ducimus rerum libero odio excepturi. Error aut nihil illum sapiente. Exercitationem est reprehenderit explicabo suscipit. Enim et et mollitia illum quas molestiae nihil.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Instrument Sales Representative'),
(36, 'Dr.', 'Et autem accusantium eos aut. Quam et quod nobis voluptate praesentium.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Substation Maintenance'),
(37, 'Miss', 'Id unde qui velit est. Assumenda veritatis repudiandae rerum enim qui et. Dicta tempora sit consequatur molestias non. Quia est et libero ut adipisci. Ea qui nesciunt eius ullam in incidunt esse.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Actor'),
(38, 'Dr.', 'Molestiae fugit dolorem sapiente quis rerum at. Doloremque rerum voluptates et rerum deleniti suscipit est totam. Qui molestiae tenetur delectus incidunt error. Nam expedita sed rem sit.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Drilling and Boring Machine Tool Setter'),
(39, 'Dr.', 'Explicabo accusamus alias et a quibusdam. Nesciunt nihil eos odio necessitatibus iure dolorem ut architecto. Ut quam necessitatibus eveniet inventore.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Wind Instrument Repairer'),
(40, 'Prof.', 'Nesciunt aut dignissimos omnis est voluptatum voluptatem. Et eveniet in eos consequuntur et odio delectus. Mollitia saepe et ab qui vitae officiis adipisci.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Locomotive Firer'),
(41, 'Mr.', 'Et incidunt sit dignissimos error quia. Iure unde ullam recusandae rem. Saepe qui ut unde temporibus. Sit autem quis ipsum aut fuga temporibus.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Nursing Aide'),
(42, 'Dr.', 'Harum natus et deserunt eligendi consectetur. Corporis deserunt illo cumque. Tenetur et cum tempora iusto. Et ullam dolorem deserunt quis expedita sapiente natus.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Film Laboratory Technician'),
(43, 'Miss', 'Qui fugit rerum aut non magni. Iste sit recusandae quas expedita voluptatem est.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Real Estate Appraiser'),
(44, 'Mr.', 'Nulla perspiciatis aut facilis vitae omnis blanditiis eligendi. Qui aut modi mollitia sint beatae. Similique qui deleniti dolorem fuga. Sunt rerum et repudiandae omnis.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Fire Investigator'),
(45, 'Dr.', 'Dolorem sed non libero nostrum cupiditate. Vel laborum maxime neque ut dolor consequuntur nihil ut. Voluptates sit non sit facere nam.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Tile Setter OR Marble Setter'),
(46, 'Dr.', 'Omnis aut beatae dolorem magnam necessitatibus maiores. Nesciunt natus sit possimus voluptatibus consequuntur soluta cumque.', '2020-09-16 00:08:09', '2020-09-16 00:08:09', NULL, 'Radio Mechanic'),
(47, 'Prof.', 'Id impedit veniam accusamus sunt dignissimos ut. Dolor tenetur perferendis ipsum adipisci ullam. Voluptatum velit atque aut odio omnis porro.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Electrical Sales Representative'),
(48, 'Mrs.', 'Quia in eaque occaecati eius. Accusamus nesciunt facere sed distinctio architecto debitis. Aut eum reprehenderit perspiciatis ad in sunt.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Designer'),
(49, 'Ms.', 'Quaerat laboriosam dolorem omnis odio quos dicta neque. Voluptas quibusdam quo perferendis omnis. Beatae deserunt qui nemo debitis aut in similique.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Claims Adjuster'),
(50, 'Miss', 'Commodi placeat hic reprehenderit perferendis. Quia asperiores occaecati totam quibusdam. Eos inventore recusandae quam praesentium dignissimos.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Tractor Operator'),
(51, 'Prof.', 'Cumque non vel ut at perferendis voluptas asperiores. Commodi minima debitis dignissimos sed eveniet officia illo. Qui quaerat repellendus sed qui et voluptas velit.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'CTO'),
(52, 'Ms.', 'Necessitatibus aut veritatis cumque nemo. Fuga eos debitis corrupti dolor ratione officia voluptatem. Sit sed voluptatem quo.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Molding Machine Operator'),
(53, 'Mr.', 'Blanditiis sit delectus reprehenderit dolorem. Tempore non et quis dolores laudantium eos ullam quia. Qui blanditiis debitis est ipsa reiciendis ratione velit.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Paving Equipment Operator'),
(54, 'Prof.', 'Consectetur accusantium quia vitae omnis qui nisi et quia. Et ex vel facilis omnis quo. Eligendi eveniet eos unde et modi. Omnis voluptatem quod et.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Artist'),
(55, 'Prof.', 'Enim asperiores laborum sit veniam. Illo adipisci nesciunt sit et quis. Vitae adipisci et qui consequatur aut.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Home'),
(56, 'Mrs.', 'Dolorum sunt autem velit delectus quos. In itaque odio quis autem. Corporis et qui recusandae nihil alias quam dolor. Incidunt minima ut et et omnis.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Protective Service Worker'),
(57, 'Dr.', 'Fuga ipsam vero et expedita nulla voluptatibus. Vitae excepturi velit sapiente ipsam eaque quia. Fugiat quis ipsum sed ea est rerum.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Construction Equipment Operator'),
(58, 'Dr.', 'Quae a repellendus commodi nemo atque at praesentium. Cupiditate odio suscipit praesentium labore mollitia et consequatur. Est in illo voluptas corrupti aliquid rerum. Qui ut et voluptatum assumenda.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Substation Maintenance'),
(59, 'Prof.', 'Harum aliquid animi deleniti fugit laudantium. Autem aut exercitationem tenetur iusto eos accusantium nulla. Quis perferendis officiis molestiae veritatis ratione iure omnis.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Medical Records Technician'),
(60, 'Prof.', 'Id ut voluptatum veritatis sit ipsam autem. Autem nemo labore natus. Debitis numquam sint rerum suscipit.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Electrical Drafter'),
(61, 'Mr.', 'Sint molestiae quo velit est sint. Eos exercitationem aut et veritatis voluptate optio. Explicabo est dolorem autem deleniti minima voluptatem omnis. Sed alias cupiditate assumenda aut.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Orthodontist'),
(62, 'Ms.', 'Consequatur nesciunt temporibus eum vel nostrum nisi. Ducimus itaque rerum nobis alias. Ipsam reprehenderit dignissimos nihil in similique pariatur.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Detective'),
(63, 'Prof.', 'Veritatis culpa cupiditate sit. Dolores quasi eius et illum dolorem. Cumque et facilis cum dolorem. Voluptas tempora maiores laborum est officia asperiores odio.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'State'),
(64, 'Mr.', 'Veniam mollitia sit quae sunt dignissimos possimus sunt. Voluptate natus minima facere omnis. Error qui at quas harum quia maiores.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Weapons Specialists'),
(65, 'Prof.', 'Nemo molestias fugit odit sit eius nisi et ducimus. Odit maxime et dignissimos atque temporibus sint saepe. Aut aut illo est sit ut soluta molestiae aut. Aliquid modi est et. Tempora qui ut eum et.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Plumber'),
(66, 'Prof.', 'Soluta ut ducimus sapiente nulla ab qui odit minus. Facilis quos vel omnis facilis vel aut sit. Nobis beatae quo eum.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Statement Clerk'),
(67, 'Dr.', 'Ea sit est occaecati nostrum rerum. Debitis quas ut voluptas facilis. Excepturi nemo similique earum consequatur ut deleniti voluptatibus. Perferendis ea eligendi eius consequatur.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Packer and Packager'),
(68, 'Mr.', 'Recusandae ut nostrum consequatur nesciunt quia sit. Laudantium sint molestias quod aperiam. Reiciendis dicta laudantium voluptatibus ut officia nihil. Eligendi ipsum similique id id.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Correspondence Clerk'),
(69, 'Prof.', 'Quia ipsum facilis aspernatur sint neque rerum. Odit saepe ratione voluptas quibusdam incidunt eius. Vitae quo eligendi et commodi.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Conveyor Operator'),
(70, 'Prof.', 'Qui et ullam vitae accusamus eius. Vitae maxime veniam dolorem qui itaque dolorem ut. Alias non asperiores nobis officiis.', '2020-09-16 00:08:10', '2020-09-16 00:08:10', NULL, 'Professional Photographer'),
(71, 'Dr.', 'Aliquam officia quae sint. Est fugit quasi possimus facere. Deserunt reiciendis voluptatem consequatur sit.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Gaming Supervisor'),
(72, 'Mr.', 'Dignissimos itaque quo culpa sint laboriosam. Culpa est error qui et accusantium voluptas asperiores. Reiciendis rerum et aspernatur asperiores non laborum.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Food Preparation'),
(73, 'Mr.', 'Ipsa in qui eum tenetur odit molestiae. Aut qui enim nemo voluptates.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Hazardous Materials Removal Worker'),
(74, 'Ms.', 'Ut enim sunt repudiandae eum beatae. Soluta commodi cumque quae est nisi odit odit. Dolor debitis veritatis quam esse. Sequi nostrum a omnis aliquam assumenda nesciunt id.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Washing Equipment Operator'),
(75, 'Dr.', 'Fugit enim illum unde expedita perferendis. Eveniet ea quae ut fugit cupiditate vel nam autem. Cupiditate provident quibusdam quia ratione. Porro eos nesciunt dicta rem veniam illo qui.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'RN'),
(76, 'Prof.', 'Non quisquam sint facilis blanditiis dolores et. Ut ipsam saepe rerum ut est et architecto. Velit optio nulla et et et. Est et harum quibusdam deleniti.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Pipelaying Fitter'),
(77, 'Mr.', 'Itaque voluptates qui et ut. Incidunt modi sit est ut aut quia harum. Quas nostrum officiis labore odit officiis. Omnis velit aliquam expedita dolorum quis rerum.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Administrative Law Judge'),
(78, 'Ms.', 'Sed non soluta temporibus animi iste. Ad voluptatem provident earum iure. Qui ipsum voluptatibus autem molestias.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Medical Transcriptionist'),
(79, 'Miss', 'Voluptas itaque vel ad enim dolorem doloribus distinctio. Nam omnis rerum distinctio debitis. Quae veritatis ullam nobis id error molestiae.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Education Administrator'),
(80, 'Mr.', 'Dolores illo et totam voluptatum. Enim enim commodi voluptatibus facere ut a. Ratione nostrum quibusdam quasi est cum. Doloribus eum dignissimos explicabo in expedita.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Floral Designer'),
(81, 'Prof.', 'Nisi aut itaque excepturi. Et dolor sit et voluptate id. Voluptates dolores eius deserunt officiis fuga. Ea esse vero aperiam pariatur illo.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Clinical School Psychologist'),
(82, 'Miss', 'Labore quo quas tempore deleniti totam officiis ullam. Ut natus architecto repellendus ut sint expedita ut consequuntur.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Baker'),
(83, 'Dr.', 'Vitae sunt earum et vitae iure. Et et aut pariatur molestiae. Iure officiis aut ut consequatur laboriosam.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Cooling and Freezing Equipment Operator'),
(84, 'Dr.', 'Suscipit doloremque est sed ea expedita ut ratione. Velit nostrum fugit et eveniet quia. Et magnam consequatur minima praesentium soluta in. Animi in fuga voluptatibus minima fuga rerum laborum.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Grinder OR Polisher'),
(85, 'Dr.', 'Totam iusto tempore et. Officia eius autem vero corrupti et ea impedit. Vitae est quae repudiandae ut nihil dolores maxime cum. Esse vel ut labore.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Forging Machine Setter'),
(86, 'Prof.', 'Rem et numquam aut tenetur voluptates. Libero quam id ut velit dolore. Labore praesentium similique et in accusamus architecto vel.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Utility Meter Reader'),
(87, 'Miss', 'Reprehenderit ullam numquam quos repellendus quia. Dolor dolor voluptate voluptatem voluptas quo enim. Et sequi placeat et explicabo in consectetur. Ea consequuntur laboriosam qui quasi est dignissimos placeat.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Excavating Machine Operator'),
(88, 'Prof.', 'Esse aperiam nam enim repellat beatae aut. Autem quos quia eum voluptatum officiis quos id. Ullam ut cum eum deleniti sit sapiente qui. Perferendis voluptas est et facilis sint ut molestias.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Night Security Guard'),
(89, 'Miss', 'Praesentium reiciendis placeat odit. Pariatur commodi velit iure eum velit. Praesentium sunt totam quidem culpa aut est. Culpa quaerat iusto sit quo amet ex nesciunt explicabo.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Curator'),
(90, 'Ms.', 'Praesentium sunt cupiditate soluta deleniti placeat quia aspernatur. Neque cumque voluptates sit et commodi. Suscipit ducimus quis facere totam accusamus repellendus quia repellat. Ipsam numquam nihil rerum ut voluptatibus eum. Ea optio ipsam officia.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Petroleum Pump System Operator'),
(91, 'Mr.', 'Quae aut laborum cum et incidunt eos. Non pariatur inventore optio eum iste labore qui quis. Ut exercitationem quasi non. Nulla vitae consectetur dignissimos aliquid aut temporibus. Dignissimos enim eos quisquam excepturi sint molestiae ipsum.', '2020-09-16 00:08:11', '2020-09-16 00:08:11', NULL, 'Tax Preparer'),
(92, 'Prof.', 'Provident reiciendis ratione totam quo. Sunt nesciunt et nostrum et voluptas atque. Odio voluptatem eaque cupiditate numquam.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Nuclear Monitoring Technician'),
(93, 'Dr.', 'Eum aut delectus molestias incidunt laboriosam corporis atque. Quidem quidem ut voluptatem et commodi. Voluptate et amet perspiciatis et sunt quam ea.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Actuary'),
(94, 'Miss', 'Dolores perspiciatis qui et delectus magni voluptatibus. Sapiente est laboriosam corrupti reprehenderit quas et. Dolorem ut aperiam et consequatur nam rerum. Harum velit ut rerum id. Eaque consequatur at sit dolores non.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Casting Machine Set-Up Operator'),
(95, 'Dr.', 'Occaecati consequatur ipsam sit molestiae. Fugit deserunt consequatur ut quidem laborum deserunt. Voluptates corporis natus accusamus voluptatem. Nemo doloremque eum officiis accusamus.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Medical Technician'),
(96, 'Miss', 'Dolorum eveniet et non ut ea eveniet doloremque. Reiciendis molestiae impedit magni consequatur deleniti et. Odio omnis exercitationem ut aut. Illo error exercitationem deserunt nemo.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Precision Printing Worker'),
(97, 'Miss', 'Dolorem ab magnam quas vel aut vitae. Ducimus impedit molestias occaecati aliquid. Cumque quidem magni qui quasi excepturi tempore sint. Vel odit reprehenderit error qui est.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Packer and Packager'),
(98, 'Mr.', 'Et amet et sed qui. Provident omnis id fuga excepturi beatae atque assumenda. Recusandae et quod hic ipsam perspiciatis consectetur. Perferendis tempore qui eaque et maiores sit adipisci.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'HVAC Mechanic'),
(99, 'Prof.', 'Excepturi hic et aut sint voluptas suscipit. Voluptatem temporibus est consequatur sed perspiciatis. Velit et incidunt qui quis quaerat.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Industrial Engineer'),
(100, 'Prof.', 'Occaecati quis vitae quibusdam praesentium dolores non consequatur. Officiis explicabo et aut. Hic modi praesentium rem est reiciendis. Voluptates omnis architecto quas inventore ea explicabo omnis ea.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Instrument Sales Representative'),
(101, 'Miss', 'Mollitia et ullam error doloribus qui perferendis libero. Dolorum velit modi ab provident. Commodi molestias quae aut aut reprehenderit quia. Deleniti modi accusamus incidunt adipisci rerum repudiandae rem.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Welding Machine Setter'),
(102, 'Mr.', 'Magni sint fugiat a. Quae id nesciunt dolore non ex ut. Adipisci voluptatem et dignissimos amet facere.', '2020-09-16 00:08:12', '2020-09-16 00:08:12', NULL, 'Zoologists OR Wildlife Biologist');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_16_045115_create_blog_table', 1),
(10, '2020_09_16_060051_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
